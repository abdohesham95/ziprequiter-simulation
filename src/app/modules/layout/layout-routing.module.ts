import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { LayoutComponent } from './layout/layout.component';

import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: '', redirectTo: 'full/home', pathMatch: 'full' },

  {
    path: 'full',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent,
        
      },
      
      

      
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutRoutingModule {}
