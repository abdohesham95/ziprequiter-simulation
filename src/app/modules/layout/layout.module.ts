import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// components
import { HomeComponent } from './pages/home/home.component';




@NgModule({
  declarations: [
    LayoutComponent,
    HomeComponent,
  
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    NgbModule,
    FontAwesomeModule,
   


  ],


  providers: [
    
  ]
})
export class LayoutModule { }
