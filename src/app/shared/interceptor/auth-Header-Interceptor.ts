import { Observable, EMPTY, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { retryWhen, delay, take, tap, map, catchError, retry } from 'rxjs/operators';
// import { AuthService } from '../auth/auth.service';

@Injectable()

export class HttpRequestInterceptor implements HttpInterceptor {
    token: any;
    // private authService: AuthService
    constructor() {
       
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // this.token = localStorage.getItem('syncToken');
        const reqWithAuth = req.clone({
            setHeaders: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin':'*',
                // 'Authorization': `${this.token}`
            }
        })
       
        return next.handle(reqWithAuth).pipe(
            retry(1),
            catchError((error: HttpErrorResponse) => {
                return throwError(error)
            })
        )
    }

}