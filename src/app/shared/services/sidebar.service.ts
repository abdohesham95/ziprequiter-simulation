import { Injectable } from '@angular/core';
import {
  faHome,
  faIdCard,
  faNewspaper,
  faBell,
  faBars,
  faHandsHelping,
  faUsers,
  faUsersCog,
  faChartBar,
  faBalanceScale,
  faUserCheck,
  faClipboardList
} from '@fortawesome/free-solid-svg-icons';
import { RouteInfo } from '../models/routes';
@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  links:RouteInfo[] = [
    { path: 'home', title: 'home', icon: faHome ,completed:false , color:'primary' },
    { path: 'about', title: 'about', icon: faIdCard ,completed:false , color:'primary'},
    { path: 'cantact', title: 'contact', icon: faIdCard ,badge:"4" ,completed:false , color:'primary'},
    { path: 'news', title: 'news', icon: faNewspaper ,completed:false , color:'primary'},
    { path: 'notification', title: 'notification', icon: faBell ,completed:false , color:'primary'},
    { path: 'consultant', title: 'consultant', icon: faHandsHelping ,badge:"4" ,completed:false , color:'primary'},
    { path: 'users', title: 'users', icon: faUsers ,completed:false , color:'primary'},
    { path: 'admins', title: 'admins', icon: faUsersCog ,completed:false , color:'primary'},
    { path: 'Terms&Conditions', title: 'Terms&Conditions', icon: faBalanceScale ,completed:false , color:'primary'},
    { path: 'plans', title: 'plans', icon: faClipboardList ,completed:false , color:'primary'},
    { path: 'features', title: 'features', icon: faClipboardList ,completed:false , color:'primary'},


  ];
  // icons
  faHome = faHome;
  faIdCard = faIdCard;
  faNewspaper = faNewspaper;
  faBell = faBell;
  faBars = faBars;
  faHandsHelping = faHandsHelping;
  faUsers = faUsers;
  faUsersCog = faUsersCog;
  faChartBar = faChartBar;
  faBalanceScale=faBalanceScale;
  faUserCheck=faUserCheck;
  faClipboardList=faClipboardList;
  constructor() { }

  get routes(){
    return [...this.links]
  }
}
