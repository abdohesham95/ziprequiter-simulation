import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActivateEmailComponent } from './components/activate-email/activate-email.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { LoginComponent } from './components/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'login' },
  
  { path: 'login', component: LoginComponent },
  { path: 'Forget-password', component:   ForgetPasswordComponent},
  { path: 'change-password', component:   ChangePasswordComponent},
  { path: 'activate-email', component:   ActivateEmailComponent},
  { path: 'register',loadChildren: () => import('./components/register/register.module').then(m => m.RegisterModule)},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
