import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,

} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
   //active button switching
   toggleRole:string ='employer';


   form:FormGroup;
   
   constructor(private _formBuilder:FormBuilder) { 
     this.form = this._formBuilder.group({
       email: [''],
       password: [''],
     })
   }
 
   ngOnInit(): void {
   }
   submitForm(){

   }
   loginAsemployer(){
     this.toggleRole = 'employer';
     console.log("employer")
   }
   loginAsSeeker(){
     this.toggleRole = 'seeker';
     console.log("seeker")

   }
   
}
