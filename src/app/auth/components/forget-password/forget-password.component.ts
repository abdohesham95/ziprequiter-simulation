import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  form:FormGroup
  constructor(private _formBuilder:FormBuilder) { 
    this.form = this._formBuilder.group({
      email: [''],
      platformType:['']
    })
  }

  ngOnInit(): void {
  }
  submitForm(){

  }

}
