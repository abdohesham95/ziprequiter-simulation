import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-activate-email',
  templateUrl: './activate-email.component.html',
  styleUrls: ['./activate-email.component.css']
})
export class ActivateEmailComponent implements OnInit {

  form:FormGroup;
  constructor(private _formBuilder:FormBuilder) { 
    this.form = this._formBuilder.group({
      email: [''],
      platformType:['']
    })
  }
  ngOnInit(): void {
  }
  submitForm(){

  }
}
