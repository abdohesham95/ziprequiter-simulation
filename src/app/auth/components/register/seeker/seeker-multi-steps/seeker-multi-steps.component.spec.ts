import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeekerMultiStepsComponent } from './seeker-multi-steps.component';

describe('SeekerMultiStepsComponent', () => {
  let component: SeekerMultiStepsComponent;
  let fixture: ComponentFixture<SeekerMultiStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeekerMultiStepsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeekerMultiStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
