import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpMultiStepsComponent } from './employer/emp-multi-steps/emp-multi-steps.component';
import { EmployerComponent } from './employer/employer.component';
import { SeekerMultiStepsComponent } from './seeker/seeker-multi-steps/seeker-multi-steps.component';

const routes: Routes = [

  {path:'employer' , component:EmployerComponent},
  {path:'employer/emp-multi-Steps' , component:EmpMultiStepsComponent},

  {path:'seeker/seeker-multi-Steps' , component:SeekerMultiStepsComponent},

  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterRoutingModule { }
