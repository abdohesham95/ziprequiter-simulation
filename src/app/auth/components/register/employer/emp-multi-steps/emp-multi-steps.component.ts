import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-multi-steps',
  templateUrl: './emp-multi-steps.component.html',
  styleUrls: ['./emp-multi-steps.component.scss'],
})
export class EmpMultiStepsComponent implements OnInit {
  firstForm: boolean = true;
  secondForm: boolean = false;
  thirdForm: boolean = false;
  forthForm: boolean = false;
  fifthForm: boolean = false;

  active = 1;
activeLink:boolean=true
  constructor(private router:Router) {}

  firstRegisterForm = new FormGroup({
    name: new FormControl(null, Validators.required),
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
  });
  get f(): { [key: string]: AbstractControl } {
    return this.firstRegisterForm.controls;
  }
  
  secondRegisterForm = new FormGroup({
    compName: new FormControl(null, Validators.required),
    jopsNo: new FormControl(null, Validators.required),
    hireNeeds: new FormControl(null, Validators.required),
    compWebsite: new FormControl(null),

  });
  get f2(): { [key: string]: AbstractControl } {
    return this.secondRegisterForm.controls;
  }
  thirdRegisterForm = new FormGroup({
    title: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
    zipCode: new FormControl(null, Validators.required),
    tracksystem: new FormControl(null, Validators.required),
    salary: new FormControl(null, Validators.required),
    social: new FormControl(null),
  });
  get f3(): { [key: string]: AbstractControl } {
    return this.thirdRegisterForm.controls;
  }

  forthRegisterForm = new FormGroup({
    title: new FormControl(null, Validators.required),
    jobLocation: new FormControl(null, Validators.required),
    street: new FormControl(null, Validators.required),
    hireRemte: new FormControl(null, Validators.required),
    EmploymentType: new FormControl(null, Validators.required),
    Description: new FormControl(null, Validators.required),
    reason: new FormControl(null, Validators.required),


    
  });
  get f4(): { [key: string]: AbstractControl } {
    return this.forthRegisterForm.controls;
  }
  fifthRegisterForm = new FormGroup({
    fullName: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
    street: new FormControl(null, Validators.required),
    blockNum: new FormControl(null, Validators.required),
    city: new FormControl(null,Validators.required),
    zipCode: new FormControl(null, Validators.required),
    country: new FormControl(null, Validators.required),
    expMonth: new FormControl(null, Validators.required),
    expYear: new FormControl(null, Validators.required),
    cvv: new FormControl(null, Validators.required),

  });
  get f5(): { [key: string]: AbstractControl } {
    return this.fifthRegisterForm.controls;
  }

  ngOnInit(): void {}
  submitFristForm() {
    this.firstForm=false;
    this.secondForm=true;
    console.log(this.firstRegisterForm.value);
  }
  submitSecondForm() {
    this.secondForm=false;
    this.thirdForm=true;
    console.log(this.secondRegisterForm.value);
  }
  submitThirdForm() {
    this.thirdForm=false;
    this.forthForm=true;
    console.log(this.thirdRegisterForm.value);
  }
  submitForthForm() {
    this.forthForm=false;
    this.fifthForm=true;
    console.log(this.forthRegisterForm.value);
  }
  submitFifthForm() {
 this.router.navigateByUrl('/')
    console.log(this.fifthRegisterForm.value);
  }
}
