import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpMultiStepsComponent } from './emp-multi-steps.component';

describe('EmpMultiStepsComponent', () => {
  let component: EmpMultiStepsComponent;
  let fixture: ComponentFixture<EmpMultiStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpMultiStepsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpMultiStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
