import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  form:FormGroup;
  constructor(private _formBuilder:FormBuilder) { 
    this.form = this._formBuilder.group({
      email: [''],
      currentPassword: [''],
      newPassword:['']
    })
  }

  ngOnInit(): void {
  }
  submitForm(){

  }
}
